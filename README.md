Idempiere exposes the client ID in the context so it's possible to dynamically pick the name of the client so no need to have hard coded reports for each client.
