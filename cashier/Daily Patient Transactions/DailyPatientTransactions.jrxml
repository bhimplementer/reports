<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.5.1.final using JasperReports Library version 6.5.1  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Daily Patient Transactions" pageWidth="595" pageHeight="850" whenNoDataType="AllSectionsNoDetail" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="0b72be3f-d755-462a-8a67-0282dc90d574">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="postgre UAT"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<parameter name="beginDate" class="java.util.Date"/>
	<parameter name="endDate" class="java.util.Date"/>
	<parameter name="AD_ORG_ID" class="java.lang.Integer" isForPrompting="false"/>
	<parameter name="AD_CLIENT_ID" class="java.lang.Integer" isForPrompting="false"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT 
	patientsummary.name AS GivenName,
    patientsummary.created AS Created,
    patientsummary.grandtotal AS GrandTotal,
    patientsummary.totalopenbalance AS TotalOpenBalance,
    patientsummary.ad_client_id AS AD_Client_ID,
    patientsummary.ad_org_id AS AD_Org_ID,
    patientsummary.totalpayments AS TotalPayments,
    patientsummary.c_orderline_id AS C_Orderline_ID,
    patientsummary.c_invoiceline_id AS C_Invoiceline_ID,
    patientsummary.c_invoice_id AS C_Invoice_ID,
    patientsummary.c_payment_id AS C_Payment_ID,
    patientsummary.client_name AS Client_Name,
    patientsummary.tendertype AS PaymentMode,
    CASE 
    	WHEN patientsummary.totalpayments > patientsummary.grandtotal 
    		THEN patientsummary.grandtotal
   	 	ELSE patientsummary.totalpayments 
   	 END AS NetPayment
    FROM bh_patient_transactions_v as patientsummary
WHERE ad_client_id =  $P{AD_CLIENT_ID}   
AND created BETWEEN $P{beginDate} AND  $P{endDate}]]>
	</queryString>
	<field name="GivenName" class="java.lang.String"/>
	<field name="Created" class="java.sql.Timestamp"/>
	<field name="GrandTotal" class="java.lang.Float"/>
	<field name="TotalOpenBalance" class="java.lang.Float"/>
	<field name="AD_Client_ID" class="java.lang.Integer"/>
	<field name="AD_Org_ID" class="java.lang.Integer"/>
	<field name="TotalPayments" class="java.lang.Float"/>
	<field name="C_Orderline_ID" class="java.lang.Integer"/>
	<field name="C_Invoiceline_ID" class="java.lang.Integer"/>
	<field name="C_Invoice_ID" class="java.lang.Integer"/>
	<field name="C_Payment_ID" class="java.lang.Integer"/>
	<field name="Client_Name" class="java.lang.String"/>
	<field name="PaymentMode" class="java.lang.String"/>
	<field name="NetPayment" class="java.lang.Float"/>
	<variable name="TotalPayments" class="java.lang.Float" calculation="Sum">
		<variableExpression><![CDATA[$F{NetPayment}]]></variableExpression>
	</variable>
	<variable name="TotalCharges" class="java.lang.Float" calculation="Sum">
		<variableExpression><![CDATA[$F{GrandTotal}]]></variableExpression>
	</variable>
	<variable name="TotalChange" class="java.lang.Float">
		<variableExpression><![CDATA[$V{TotalPayments} - $V{TotalCharges}]]></variableExpression>
	</variable>
	<group name="Group1">
		<groupExpression><![CDATA[$F{PaymentMode}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="46" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="555" height="46" uuid="c877cca5-9599-4880-b36b-b97a665dccdd"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="24"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Client_Name}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="27" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="555" height="25" uuid="833f8d94-32f0-4b30-9c56-995682b3e0bf"/>
				<textElement textAlignment="Center">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Patient Transactions"]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="70" height="16" uuid="de140269-bbf7-4026-a3ea-6c1254aaaf56"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="75" y="0" width="82" height="16" uuid="47474ccf-9801-42d2-b3c8-0e9dd5805389"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Patient Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="168" y="0" width="71" height="16" uuid="3353f64b-8d84-48c0-9700-8384cc14c569"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Charges]]></text>
			</staticText>
			<staticText>
				<reportElement x="236" y="0" width="78" height="16" uuid="08138a37-96d1-4fca-86d2-017b6d54dc6c"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Net Payment]]></text>
			</staticText>
			<staticText>
				<reportElement x="413" y="0" width="119" height="16" uuid="3416f17b-2346-4043-b1ba-336a08d4485b"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Outstanding Balance]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="13" width="530" height="1" uuid="e9ae85de-7d27-47b6-9abb-dfd5c972ef8d"/>
			</line>
			<staticText>
				<reportElement x="317" y="1" width="85" height="16" uuid="abc0041b-c7a0-4ce4-99e1-8af36b1a89b8"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Payment Mode]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="21">
			<textField pattern="d MMM, yyyy">
				<reportElement x="0" y="0" width="75" height="16" uuid="089ae30e-2a51-4419-ab4a-99216852769d"/>
				<textFieldExpression><![CDATA[$F{Created}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="77" y="0" width="91" height="16" uuid="e562d461-d810-4383-a579-885fa2ae3251"/>
				<textFieldExpression><![CDATA[$F{GivenName}.toUpperCase()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="187" y="0" width="57" height="16" uuid="55e65eeb-2bd6-4d0a-839b-215119463781"/>
				<textElement textAlignment="Left"/>
				<textFieldExpression><![CDATA[$F{GrandTotal}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="254" y="0" width="58" height="16" uuid="b112dbde-59e8-4a99-ba20-0b1b16c22f2a"/>
				<textFieldExpression><![CDATA[$F{NetPayment}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="439" y="0" width="71" height="16" uuid="29817303-1a87-45f4-9cb5-3927a9925476"/>
				<textFieldExpression><![CDATA[$F{TotalOpenBalance}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="346" y="1" width="65" height="16" uuid="b6d94475-40b0-4140-8d35-a138f3630209"/>
				<textFieldExpression><![CDATA[$F{PaymentMode}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="0" y="3" width="379" height="16" uuid="6c430987-cd11-40be-bd9d-2077e233745f"/>
				<textFieldExpression><![CDATA["Generated at " + new SimpleDateFormat("yyyy-MMM-dd HH:mm").format(new Date())]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="522" y="3" width="33" height="16" uuid="7a9813a0-b37a-4dcb-a749-b1725b610579"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="431" y="3" width="87" height="16" uuid="f96d7780-6357-4924-b519-27f72a508d29"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="61" splitType="Stretch">
			<subreport>
				<reportElement x="350" y="6" width="163" height="38" uuid="a9f1a567-e261-40b5-9963-931fc4f96fd4"/>
				<subreportParameter name="AD_ORG_ID">
					<subreportParameterExpression><![CDATA[$P{AD_ORG_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="AD_CLIENT_ID">
					<subreportParameterExpression><![CDATA[$P{AD_CLIENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="beginDate">
					<subreportParameterExpression><![CDATA[$P{beginDate}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="endDate">
					<subreportParameterExpression><![CDATA[$P{endDate}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="TotalPayments">
					<subreportParameterExpression><![CDATA[$V{TotalPayments}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="TotalCharges">
					<subreportParameterExpression><![CDATA[$V{TotalCharges}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="TotalChange">
					<subreportParameterExpression><![CDATA[$V{TotalChange}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "DailyPatientTransactionPayments.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
	<noData>
		<band height="21">
			<staticText>
				<reportElement x="0" y="4" width="306" height="16" uuid="e8da193c-2a64-44be-96ca-7416132f7400"/>
				<textElement textAlignment="Left">
					<font isBold="false" isItalic="true"/>
				</textElement>
				<text><![CDATA[No data was found to display on the report.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
